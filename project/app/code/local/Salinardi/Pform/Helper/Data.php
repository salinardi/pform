<?php

/**
 * Salinardi_Pform
 */

/**
 * class Salinardi_Pform_Helper_Data
 *
 * Main Helper.
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Salinardi_Pform_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * getConfigData
     *
     * Returns the value for the request configuration
     * @param string $data
     * @return string
     */
    public function getConfigData($data)
    {
        return Mage::getStoreConfig('salinardi_pform/' . $data);
    }

    /**
     * isEnabled
     *
     * Returns true if the module is enabled to be displayed
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->getConfigData('configuration/enabled');
    }
}
