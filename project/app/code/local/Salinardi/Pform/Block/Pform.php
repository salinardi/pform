<?php

/**
 * Salinardi_Pform
 */

/**
 * class Salinardi_Pform_Block_Helloworld
 *
 * Main Block.
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Salinardi_Pform_Block_Pform extends Mage_Core_Block_Template
{
    /**
     * isEnabled
     *
     * Retuns true if the module is enable
     * @return boolean
     */
    public function isEnebled()
    {
        return Mage::helper('salinardi_pform')->isEnabled();
    }

    /**
     * getField
     *
     * returns the data from the setting data
     * @param string
     * @return string
     */
    public function getField($fieldName)
    {
        if ($this->isEnebled()) {
            return Mage::helper('salinardi_pform')->getConfigData('personal_form/'.$fieldName);
        }
        return false;
    }

    /**
     * getPostField
     *
     * returns the data from post data of the form
     * @param string
     * @return string
     */
    public function getPostField($param) {
        return Mage::app()->getRequest()->getParam($param);
    }
}
