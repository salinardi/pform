<?php
class Salinardi_Pform_Block_ConfigSelect_GridForm extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    protected $_itemInputType;

    public function _prepareToRender()
    {


        $this->addColumn('attributejson', array(
            'label' => $this->__('Input label'),
            'style' => 'width:150px',
        ));

        //Mage::log('SelectAttributes.php - _prepareToRender()');
        $this->addColumn('attributename', array(
            'label' => $this->__('Input Type'),
            'renderer' => $this->_getRendererInputType(),
        ));

//        $this->addColumn('attributecheck', array(
//            'label' => $this->__('Required'),
//            'renderer' => $this->_getRendererRequired(),
//        ));

        //$this->_addAfter = false;
        //$this->_addButtonLabel = Mage::helper('findifyfeed')->__('Add');
        $this->_addButtonLabel = $this->__('Add');
    }

    protected function  _getRendererInputType()
    {
        //Mage::log('SelectAttributes.php - _getRenderer()');
        if (!$this->_itemInputType) {
            //Mage::log('SelectAttributes.php - _getRenderer() - if (!$this->_itemRenderer)');
            $this->_itemInputType = $this->getLayout()->createBlock(
                //'pform_block_configSelect_fillFields_fillSelectInputType',
                'Salinardi_Pform_Block_ConfigSelect_FillFields_FillSelectInputType',
                'primo',
                //array('is_render_to_js_template' => true)
                array('ciao')
            );
        }
        return $this->_itemInputType;
    }


    protected function _prepareArrayRow(Varien_Object $row)
    {
        //Mage::log('SelectAttributes.php - _prepareArrayRow()');
        $row->setData(
            'option_extra_attr_' . $this->_getRenderer()
                ->calcOptionHash($row->getData('attributename')),
            'selected="selected"'
        );
    }

}