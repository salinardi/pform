<?php
class Salinardi_Pform_Block_ConfigSelect_FillFields_FillSelectInputType extends Mage_Core_Block_Html_Select
{

    public function _toHtml()
    {
        //$attributes = Mage::getResourceModel('catalog/product_attribute_collection')->getItems();

        $this->addOption(1, "checkbox");
        $this->addOption(2, "date");
        $this->addOption(3, "email");
        $this->addOption(4, "password");
        $this->addOption(5, "tel");
        $this->addOption(6, "text");
        $this->addOption(7, "url");
        $this->addOption(8, "range");
        $this->addOption(9, "color");


        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
