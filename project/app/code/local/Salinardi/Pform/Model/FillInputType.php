<?php
class Salinardi_Pform_Model_FillInputType
{
    public function toOptionArray()
    {
        $themes = array(
            array('value' => 'green', 'label' => 'Green'),
            array('value' => 'blue', 'label' => 'Blue'),
            array('value' => 'beige', 'label' => 'Beige'),
        );

        return $themes;
    }
}