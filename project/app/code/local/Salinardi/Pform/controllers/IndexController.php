<?php

/**
 * Salinardi_Pform
 */

/**
 * class Salinardi_Pform_controllers_IndexController
 *
 * Index .Controller
 * @author Manuel Salinardi <salinardii@libero.it>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Salinardi_Pform_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * indexAction
     *
     * Goes tu http://[domain]/[frontend-name]/index/index
     * @return void
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
        //echo "ciao";
    }

    public function goAction()
    {
        $this->loadLayout();
        $this->renderLayout();
        echo "ciao";
    }

    /**
     * resultsAction
     *
     * Goes tu http://[domain]/[frontend-name]/index/results
     * @return void
     */
    public function resultsAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

}
